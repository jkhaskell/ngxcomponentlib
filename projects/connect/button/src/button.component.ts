import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'mcui-button',
  template: `
    <button
      [disabled]="disabled"
      [style.backgroundColor]="isHover ? 'cyan' : color"
      [style.opacity]="disabled ? 0.3 : 1"
      (click)="onClick.emit()"
      (mouseover)="isHover = true"
      (mouseleave)="isHover = false"
    >
      {{ text }}
    </button>
  `,
  styles: [`
    button {
      border: none;
      color: white;
      padding: 1rem;
      cursor: pointer;
    }
  `]
})
export class ButtonComponent {
  @Input() text: string = '';
  @Input() color?: string = 'blue';
  @Input() disabled?: boolean = false;
  @Output() onClick = new EventEmitter();

  isHover = false;
}
