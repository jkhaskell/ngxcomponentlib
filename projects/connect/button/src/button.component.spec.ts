import {render, screen, fireEvent} from '@testing-library/angular'
import { ButtonComponent } from './button.component';
 
const buttonText = 'Test Button'
const onClickSpy = jest.fn()

beforeEach(async () => {
  render(ButtonComponent, {componentProperties: {text: buttonText, onClick: {emit: onClickSpy} as any }})
})

test('it should render with inputted text', async () => {
  expect(screen.getByText(buttonText)).toBeTruthy()
})

test('clicking triggers event', async () => {
  expect(onClickSpy).toHaveBeenCalledTimes(0)
  fireEvent.click(screen.getByText(buttonText))
  expect(onClickSpy).toHaveBeenCalledTimes(1)
})