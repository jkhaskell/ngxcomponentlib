import { Component, Input } from '@angular/core';

@Component({
  selector: 'mcui-button',
  template: `
    <div class="card">
      {{ text }}
    </div>
  `,
  styles: [`
    .card {
      border: 1px solid black;
      padding: 2rem;
      max-width: 4rem;
      display: flex;
      align-items: center;
      justify-content: center;
      overflow-wrap: break-word;
    }
  `]
})
export class CardComponent {
  @Input() text: string = 'Card';
}
