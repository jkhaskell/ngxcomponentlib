import {render, screen, fireEvent} from '@testing-library/angular'
import { CardComponent } from './card.component';
 
const text = 'Test Card'

beforeEach(async () => {
  render(CardComponent, {componentProperties: {text}})
})

test('it should render with inputted text', async () => {
  expect(screen.getByText(text)).toBeTruthy()
})
