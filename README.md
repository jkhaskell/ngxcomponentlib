# @connect/ui-angular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.3.

## Build

Run `ng build <project name>` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `npm run test` to execute the unit tests via [Jest](https://jestjs.io/).

## Running Storybook
Run `npm run storybook`.
